#
# Copyright (c) 2017 TOYOTA MOTOR CORPORATION
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

TEMPLATE = lib
VERSION = 0.1.0
TARGET = qtsoundmanager

HEADERS = libqtsoundmanager.h
SOURCES = libqtsoundmanager.cpp

headers.path = /usr/include
headers.files = libqtsoundmanager.h

target.path = /usr/lib

CONFIG += link_pkgconfig create_pc create_prl no_install_prl

PKGCONFIG += soundmanager

QMAKE_PKGCONFIG_NAME = libqtsoundmanager
QMAKE_PKGCONFIG_FILE = $${QMAKE_PKGCONFIG_NAME}
QMAKE_PKGCONFIG_VERSION = $${VERSION}
QMAKE_PKGCONFIG_DESCRIPTION = A wrapper interface for libsoundmanager for Qt
QMAKE_PKGCONFIG_LIBDIR = ${prefix}/lib
QMAKE_PKGCONFIG_INCDIR = ${prefix}/include
QMAKE_PKGCONFIG_REQUIRES = soundmanager
QMAKE_PKGCONFIG_DESTDIR = pkgconfig

INSTALLS += target headers

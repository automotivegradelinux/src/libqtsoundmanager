/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef LIBQTSOUNDMANAGER_H
#define LIBQTSOUNDMANAGER_H

 #include <QObject>
 #include <QVariant>
 #include <QtCore/QJsonObject>
 #include <libsoundmanager.hpp>
 #include <QString>
 #include <string>

// Note: This wrapper library will be integrated to libqtappfw

class QSoundmanager : public QObject, Soundmanager
{
    Q_OBJECT
public: // method
    explicit QSoundmanager(QObject *parent = nullptr);
    ~QSoundmanager();
    int init(int port, const QString& token);
    void subscribe(const QString event_name);
    void unsubscribe(const QString event_name);

public:

    Q_INVOKABLE int call(const QString &verb, const QJsonObject &arg);
    Q_INVOKABLE int connect(int sourceID, const QString& sinkName = "default");
    Q_INVOKABLE int connect(int sourceID, int sinkID);
    Q_INVOKABLE int disconnect(int connectionID);
    Q_INVOKABLE int ackSetSourceState(int handle, int errorcode);
    Q_INVOKABLE int registerSource(const QString& audio_role);
    Q_INVOKABLE int getListMainSinks();
    Q_INVOKABLE int getListMainSources();
    Q_INVOKABLE int getListMainConnections();
    Q_INVOKABLE int streamOpen(const QString& audio_role, int endpoint_id);
    Q_INVOKABLE int streamOpen(const QString& audio_role, const QString& endpoint_id = "default");
    Q_INVOKABLE int streamClose(int stream_id);
    Q_INVOKABLE int setStreamState(int stream_id, int mute = 0); // 0 is unmute , 1 is mute

signals:
    void reply(const QVariant &msg);
    void event(const QVariant &event, const QVariant &msg);
};


#endif /*LIBQTSOUNDMANAGER_H*/
